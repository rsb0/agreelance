# Generated by Django 2.1.7 on 2020-03-09 18:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0012_remove_profilerating_rating'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ProfileRating',
        ),
    ]
