from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from projects.models import ProjectCategory
from django.db.models import Avg
from django.contrib.auth import login, authenticate, update_session_auth_hash
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from .models import Profile, ProfileRating

from .forms import SignUpForm


def index(request):
    return render(request, 'base.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.company = form.cleaned_data.get('company')
            user.profile.country = form.cleaned_data.get('country')
            user.profile.phone_number = form.cleaned_data.get('phone_number')
            user.profile.state = form.cleaned_data.get('state')
            user.profile.city = form.cleaned_data.get('city')
            user.profile.postal_code = form.cleaned_data.get('postal_code')
            user.profile.street_address = form.cleaned_data.get('street_address')

            user.is_active = True
            user.profile.categories.add(*form.cleaned_data['categories'])
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            from django.contrib import messages
            messages.success(request, 'Your account has been created and is awaiting verification.')
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'user/signup.html', {'form': form})


@login_required
def profile_view(request, username):
    user = request.user
    profile = Profile.objects.filter(user__username=username).first()
    profile_ratings = ProfileRating.objects.filter(rated_profile=profile.user)
    avg_rating = 0.0
    has_rated = False
    user_rated = 0
    if len(profile_ratings) != 0:
        for profile_rating in profile_ratings:
            if user == profile_rating.reviewer:
                has_rated = True
                user_rated = profile_rating.rating
            avg_rating += profile_rating.rating
        avg_rating /= len(profile_ratings)
    avg_rating_float = format(avg_rating, '.2f')
    return render(request,
                  'user/profile.html',
                  {'profile': profile,
                   'avg_rating': avg_rating,
                   'has_rated': has_rated,
                   'user_rated': user_rated,
                   'avg_rating_float': avg_rating_float
                   })


@login_required
def edit_password_view(request, username):
    if request.method == 'GET':
        if request.user.username != username:
            return redirect('profile', username)
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)

        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Password successfully changed')
            return render(request, 'user/edit_password.html', {'form': form})
        else:
            messages.error(request, 'Correct errors')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'user/edit_password.html', {'form': form})


@login_required
def rate_profile_view(request, username):
    user = request.user
    profile = Profile.objects.get(user__username=username)
    if request.method == "POST" and user.id != profile.user.id:
        form_data = request.POST.dict()
        rating = form_data.get('rating')
        prev_rating = ProfileRating.objects.filter(rated_profile=profile.user, reviewer=user).first()
        if prev_rating is not None:
            prev_rating.delete()
        profile_rating = ProfileRating(reviewer=user, rated_profile=profile.user, rating=rating)
        profile_rating.save()
    return redirect('../../' + str(username))

