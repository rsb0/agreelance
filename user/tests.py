import string

from allpairspy import AllPairs
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from projects.models import Project, Task, ProjectCategory, TaskOffer
from random import choice
from user.models import Profile, ProfileRating
from unittest import skip

# Create your tests here.

# =============================================================================================================
# TASK 3 boundary value tests for signup page:
# =============================================================================================================
class SignupBoundaryValue(TestCase):
    def setUp(self):
        self.client = Client()

        self.gardening = ProjectCategory.objects.create(name="Gardening")
        self.painting = ProjectCategory.objects.create(name="Painting")
        self.programming = ProjectCategory.objects.create(name="Programming")

    # =============================================================================================================
    # Test max boundry value for all input values

    # Upper boundry value for Django.contrib.auth.model.User.username = 150 characters:
    # https://docs.djangoproject.com/en/3.0/ref/contrib/auth/
    def test_signup_max_values_test(self):
        username = 'a' * 150
        firstname = 'a' * 30
        lastname = 'a' * 30
        company = "a" * 30
        email = 'b' * 242 + "@" + 'a' * 4 + "." + "a" * 4  # max value for email 254 characters
        password = ''.join(choice(string.ascii_lowercase) for n in range(4096))  # generates random 4096 char string
        phone = '1' * 50
        country = 'a' * 50
        state = 'a' * 50
        city = 'a' * 50
        postal_code = '1' * 50
        address = 'a' * 50
        self.client.post(reverse('signup'), {
            'username': username,
            'first_name': firstname,
            'last_name': lastname,
            'company': company,
            'categories': '1',
            'categories': '2',
            'categories': '3',
            'email': email,
            'email_confirmation': email,
            'password1': password,
            'password2': password,
            'phone_number': phone,
            'country': country,
            'state': state,
            'city': city,
            'postal_code': postal_code,
            'street_address': address
        })
        self.assertEqual(User.objects.all().count(), 1)

    # =============================================================================================================
    # Test user name boundry value
    # =============================================================================================================
    def test_signup_username_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': '',
            'first_name': 'Fname',
            'last_name': 'Lname',
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_username_1_character(self):
        self.client.post(reverse('signup'), {'username': 'a',
                                             'first_name': 'Fname',
                                             'last_name': 'Lname',
                                             'company': 'Company',
                                             'categories': '1',
                                             'email': 'email@email.com',
                                             'email_confirmation': 'email@email.com',
                                             'password1': 'passord1',
                                             'password2': 'passord1',
                                             'phone_number': '12345',
                                             'country': 'Norway',
                                             'state': 'State',
                                             'city': 'city',
                                             'postal_code': '1234',
                                             'street_address': 'street'
                                             })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_username_151_characters(self):
        username = 'a' * 151
        self.client.post(reverse('signup'), {
            'username': username,
            'first_name': 'Fname',
            'last_name': 'Lname',
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        # User should not be created in database
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value first name
    # =============================================================================================================

    def test_signup_first_name_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': '',
            'last_name': 'Lname',
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        # should be created in database according to https://docs.djangoproject.com/en/3.0/ref/contrib/auth/
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_first_name_one_character(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'a',
            'last_name': 'Lname',
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_first_name_31_characters(self):
        firstname = 'a' * 31
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': firstname,
            'last_name': 'Lname',
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        # User should not be created in database
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value last name
    # =============================================================================================================

    def test_signup_last_name_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': '',
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_last_name_one_character(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'a',
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_last_name_31_characters(self):
        lastname = 'a' * 31
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': lastname,
            'company': 'Company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        # User should not be created in database
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value company
    # =============================================================================================================

    def test_signup_company_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': '',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_company_31_characters(self):
        company = 'a' * 31
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': company,
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        # User should not be created in database
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value last name
    # =============================================================================================================

    def test_signup_no_category(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'comp',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # Test max+ value for categories. Only created 3 categories. This should not work
    def test_signup_4_categories(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'categories': '2',
            'categories': '3',
            'categories': '4',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value email
    # =============================================================================================================

    def test_signup_email_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': '',
            'email_confirmation': '',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_email_min_characters_one(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'a@a.aa',
            'email_confirmation': 'a@a.aa',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_email_min_characters_two(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': '@a.aa',
            'email_confirmation': '@a.aa',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_email_min_characters_three(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'a@.aa',
            'email_confirmation': 'a@.aa',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_email_255_characters(self):
        email = 'b' * 245 + "@" + 'a' * 4 + "." + "a" * 4
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': email,
            'email_confirmation': email,
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        # User should be created
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value password
    # Password must be at least 8 characters, but should have no upper limit according to
    # https://docs.djangoproject.com/en/3.0/ref/contrib/auth/
    def test_signup_password_7_characters(self):
        password = ''.join(choice(string.ascii_lowercase) for n in range(7))
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': password,
            'password2': password,
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

        def test_signup_password_8_characters(self):
            password = ''.join(choice(string.ascii_lowercase) for n in range(8))
            self.client.post(reverse('signup'), {
                'username': 'user1',
                'first_name': 'fname',
                'last_name': 'lname',
                'company': 'company',
                'categories': '1',
                'email': 'email@email.com',
                'email_confirmation': 'email@email.com',
                'password1': password,
                'password2': password,
                'phone_number': '12345',
                'country': 'Norway',
                'state': 'State',
                'city': 'city',
                'postal_code': '1234',
                'street_address': 'street'
            })
            self.assertEqual(User.objects.all().count(), 0)

    @skip('Password should be too large according to: https://tech.marksblogg.com/passwords-in-django.html')
    def test_signup_password_4097_characters(self):
        password = ''.join(choice(string.ascii_lowercase) for n in range(4097))
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': password,
            'password2': password,
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        response = self.client.login(username='user1', password=password)
        self.assertFalse(response)
        self.assertEqual(User.objects.all().count(), 1)

    # =============================================================================================================
    # Test boundry value phone
    # =============================================================================================================

    def test_signup_phone_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_phone_1_characters(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '1',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_phone_51_characters(self):
        phone = '1' * 51
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': phone,
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value country
    # =============================================================================================================

    def test_signup_country_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': '',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_country_1_characters(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'N',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_country_51_characters(self):
        country = 'a' * 51
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': country,
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value state
    # =============================================================================================================

    def test_signup_state_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': '',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # 006/boundry_value_test_project_offers_and_signup_page
    def test_signup_state_1_characters(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 's',
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_state_51_characters(self):
        state = 'a' * 51
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': state,
            'city': 'city',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value city
    # =============================================================================================================

    def test_signup_city_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': '',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_city_1_characters(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'c',
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_city_51_characters(self):
        city = 'a' * 51
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': city,
            'postal_code': '1234',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value postal code
    # =============================================================================================================

    def test_signup_postal_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_postal_1_characters(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_postal_51_characters(self):
        postal_code = '1' * 51
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': 'city',
            'postal_code': postal_code,
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 0)

    # =============================================================================================================
    # Test boundry value address
    # =============================================================================================================

    def test_signup_address_empty_string(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'fname',
            'last_name': 'lname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '1234',
            'street_address': ''
        })
        self.assertEqual(User.objects.all().count(), 0)

    def test_signup_address_1_characters(self):
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'Norway',
            'state': 'State',
            'city': 'city',
            'postal_code': '12345',
            'street_address': 's'
        })
        self.assertEqual(User.objects.all().count(), 1)

    def test_signup_address_51_characters(self):
        address = '1' * 51
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': 'city',
            'postal_code': '12345',
            'street_address': address
        })
        self.assertEqual(User.objects.all().count(), 0)

    """***************************************************************************************************************
                        Boundry value tests for project offers page is in projects/tests.py
    ***************************************************************************************************************"""

    # =============================================================================================================
    # Task 3 IntegrationTests for new features
    # =============================================================================================================

class IntegrationTests(TestCase):
    # =============================================================================================================
    # Test creating a user, log in
    # =============================================================================================================
    def setUp(self):
        self.client = Client()
        self.gardening = ProjectCategory.objects.create(name="Gardening")
        self.painting = ProjectCategory.objects.create(name="Painting")
        self.programming = ProjectCategory.objects.create(name="Programming")
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': 'city',
            'postal_code': '12345',
            'street_address': 'street'
        })
        self.client.post(reverse('signup'), {
            'username': 'user2',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email2@email.com',
            'email_confirmation': 'email2@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': 'city',
            'postal_code': '12345',
            'street_address': 'street'
        })
        self.client.post(reverse('signup'), {
            'username': 'user3',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email2@email.com',
            'email_confirmation': 'email2@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': 'city',
            'postal_code': '12345',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 3)
        self.user_object = User.objects.get(username='user1')
        self.assertIsNotNone(self.user_object)
        self.client.login(username='user1', password='passord1')

    def test_view_own_profile_page(self):
        response = self.client.get(reverse('profile', kwargs={'username': self.user_object.username}))
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.context['profile'].user, self.user_object)

    def test_view_others_profile_page(self):
        user2 = User.objects.get(username='user2')
        response = self.client.get(reverse('profile', kwargs={'username': 'user2'}))
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.context['profile'].user, user2)

    def test_profile_no_rating(self):
        response = self.client.get(reverse('profile', kwargs={'username': self.user_object.username}))
        self.assertEquals(response.context['avg_rating'], 0.0)

    def test_rate_profile(self):
        self.client.post(reverse('rate_profile', kwargs={'username': 'user2'}), {
            'rating': 3
        })
        response = self.client.get(reverse('profile', kwargs={'username': 'user2'}))
        self.assertEquals(response.context['avg_rating'], 3.0)

    def test_rate_profile_overwrite(self):
        self.client.post(reverse('rate_profile', kwargs={'username': 'user2'}), {
            'rating': 3
        })
        self.client.post(reverse('rate_profile', kwargs={'username': 'user2'}), {
            'rating': 5
        })
        response = self.client.get(reverse('profile', kwargs={'username': 'user2'}))
        self.assertEquals(response.context['avg_rating'], 5.0)
        self.assertEquals(response.context['user_rated'], 5.0)

    def test_rate_own_profile(self):
        self.client.post(reverse('rate_profile', kwargs={'username': self.user_object.username}), {
            'rating': 3
        })
        response = self.client.get(reverse('profile', kwargs={'username': self.user_object.username}))
        self.assertEquals(response.context['avg_rating'], 0.0)

    def test_rate_profile_avg(self):
        user2 = User.objects.get(username='user2')
        user3 = User.objects.get(username='user3')
        profile_rating = ProfileRating(reviewer=user2, rated_profile=user3, rating=2)
        profile_rating.save()
        self.client.post(reverse('rate_profile', kwargs={'username': 'user3'}), {
            'rating': 5,
        })
        response = self.client.get(reverse('profile', kwargs={'username': 'user3'}))
        self.assertEquals(response.context['avg_rating'], 3.5)

    def test_rate_profile_avg_float(self):
        user2 = User.objects.get(username='user2')
        user3 = User.objects.get(username='user3')
        profile_rating = ProfileRating(reviewer=user2, rated_profile=user3, rating=2)
        profile_rating.save()
        self.client.post(reverse('rate_profile', kwargs={'username': 'user3'}), {
            'rating': 5,
        })
        response = self.client.get(reverse('profile', kwargs={'username': 'user3'}))
        self.assertEquals(response.context['avg_rating'], 3.50)

    def test_change_password_rate(self):
        response = self.client.post(reverse('edit_password', kwargs={'username': self.user_object.username}), {
            'old_password': 'passord1',
            'new_password1': 'passord1234',
            'new_password2': 'passord1234'
        })
        self.assertEquals(response.status_code, 200)
        self.client.logout()
        self.client.login(username='user1', password='passord1234')
        self.assertEqual(int(self.client.session['_auth_user_id']), self.user_object.pk)
        response2 = self.client.get(reverse('profile', kwargs={'username': self.user_object.username}))
        self.assertEquals(response2.status_code, 200)
        self.assertEquals(response2.context['profile'].user, self.user_object)


    def test_change_password_wrong_password_input(self):
        response = self.client.post(reverse('edit_password', kwargs={'username': self.user_object.username}), {
            'old_password': 'passord2',
            'new_password1': 'passord1234',
            'new_password2': 'passord1234'
        })
        self.assertEquals(response.status_code, 200)
        self.client.logout()
        self.client.login(username='user1', password='passord1')
        self.assertEqual(int(self.client.session['_auth_user_id']), self.user_object.pk)

    def test_change_password_non_matching_password_input(self):
        self.client.post(reverse('edit_password', kwargs={'username': 'user2'}), {
            'old_password': 'passord1',
            'new_password1': 'passord1235',
            'new_password2': 'passord1255'
        })
        self.client.login(username='user2', password='passord1')
        user2 = User.objects.get(username='user2')
        self.assertEqual(int(self.client.session['_auth_user_id']), user2.pk)


# =============================================================================================================
# TASK 3 2-way domain test signup page
# =============================================================================================================

class TwoWayTest(TestCase):
    def setUp(self):
        self.client = Client()

        self.gardening = ProjectCategory.objects.create(name="Gardening")
        self.painting = ProjectCategory.objects.create(name="Painting")
        self.programming = ProjectCategory.objects.create(name="Programming")

        """ All pairs combinations generated by allpairspy in user/utilstest.py
        1: ['UserOne', 'Christopher', 'Smith', 'Megacorp', 1, 'email@gmail.com', 'email@gmail.com', 'passord1', 'passord1', '81549300', 'Norway', 'N/A', 'Trondheim', '7013', 'Munkegata 1'] valid_form
        2: ['UserTwo', 'Christopher1', 'Smith1', '', 2, 'gmail.com', 'gmail.com', 'pass', 'pass', 'Phone number', 'Sweden', 'N/A', 'Stockholm', '111 52', 'Munkegata 1'] invalid form
        3: ['UserOne{}', 'Christopher1', 'Smith', '', 3, 'email@gmail.com', 'gmail.com', 'passord1', 'pass', '81549300', 'USA', 'Minnesota', 'Minneapolis', '55415', 'Jakobsgatan 10'] invalid form
        4: ['UserOne{}', 'Christopher', 'Smith1', 'Megacorp', 3, 'gmail.com', 'email@gmail.com', 'pass', 'passord1', 'Phone number', 'USA', 'Minnesota', 'Minneapolis', '111 52', '401 Chicago Avenue'] invalid form
        5: ['UserTwo', 'Christopher', 'Smith', 'Megacorp', 2, 'email@gmail.com', 'email@gmail.com', 'pass', 'pass', '81549300', 'Sweden', 'N/A', 'Stockholm', '55415', '401 Chicago Avenue'] invalid form
        6: ['UserOne', 'Christopher1', 'Smith1', '', 1, 'gmail.com', 'gmail.com', 'passord1', 'passord1', 'Phone number', 'Norway', 'N/A', 'Trondheim', '55415', '401 Chicago Avenue'] invalid form
        7: ['UserOne', 'Christopher1', 'Smith1', 'Megacorp', 2, 'email@gmail.com', 'gmail.com', 'pass', 'passord1', 'Phone number', 'USA', 'N/A', 'Stockholm', '7013', 'Jakobsgatan 10'] invalid form
        8: ['UserTwo', 'Christopher', 'Smith', '', 1, 'gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'Minnesota', 'Stockholm', '111 52', 'Jakobsgatan 10'] invalid form
        9: ['UserOne{}', 'Christopher', 'Smith', '', 1, 'gmail.com', 'gmail.com', 'pass', 'pass', 'Phone number', 'Sweden', 'N/A', 'Trondheim', '7013', 'Jakobsgatan 10'] invalid form
        10: ['UserTwo', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'passord1', 'Phone Number', 'Sweden', 'N/A', 'Minneapolis', '7013', 'Munkegata 1'] invalid form
        11: ['UserOne', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'pass', 'pass', '81549300', 'Norway', 'N/A', 'Trondheim', '111 52', 'Jakobsgatan 10'] invalid form
        12: ['UserOne{}', 'Christopher1', 'Smith1', '', 2, 'email@gmail.com', 'email@gmail.com', 'passord1', 'passord1', '81549300', 'Norway', 'Minnesota', 'Trondheim', '7013', 'Munkegata 1'] invalid form
        13: ['UserOne', 'Christopher1', 'Smith1', '', 1, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'USA', 'N/A', 'Minneapolis', '7013', 'Munkegata 1'] invalid form
        14: ['UserTwo', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'USA', 'N/A', 'Trondheim', '111 52', 'Jakobsgatan 10'] invalid form
        15: ['UserOne', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Sweden', 'Minnesota', 'Stockholm', '7013', '401 Chicago Avenue'] invalid form
        16: ['UserOne{}', 'Christopher1', 'Smith1', '', 2, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'N/A', 'Minneapolis', '55415', 'Munkegata 1'] invalid form
        17: ['UserOne{}', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'N/A', 'Minneapolis', '7013', 'Munkegata 1'] invalid form
        18: ['UserOne{}', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'N/A', 'Stockholm', '7013', 'Munkegata 1'] invalid form"""



    # 1: ['UserOne', 'Christopher', 'Smith', 'Megacorp', 1, 'email@gmail.com', 'email@gmail.com', 'passord1', 'passord1', '81549300', 'Norway', 'N/A', 'Trondheim', '7013', 'Munkegata 1'] valid form
    def test_two_way_one(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher',
            'last_name': 'Smith',
            'company': 'Megacorp',
            'categories': '1',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 1)

    # 2: ['UserTwo', 'Christopher1', 'Smith1', '', 2, 'gmail.com', 'gmail.com', 'pass', 'pass', 'Phone number', 'Sweden', 'N/A', 'Stockholm', '111 52', 'Munkegata 1'] invalid form
    def test_two_way_two(self):
        self.client.post(reverse('signup'), {
            'username': 'UserTwo',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '2',
            'email': 'gmail.com',
            'email_confirmation': 'gmail.com',
            'password1': 'pass',
            'password2': 'pass',
            'phone_number': 'Phone number',
            'country': 'Sweden',
            'state': 'N/A',
            'city': 'Stockholm',
            'postal_code': '111 52',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # 3: ['UserOne{}', 'Christopher1', 'Smith', '', 3, 'email@gmail.com', 'gmail.com', 'passord1', 'pass', '81549300', 'USA', 'Minnesota', 'Minneapolis', '55415', 'Jakobsgatan 10'] invalid form
    def test_two_way_three(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne{}',
            'first_name': 'Christopher1',
            'last_name': 'Smith',
            'company': '',
            'categories': '3',
            'email': 'email@gmail.com',
            'email_confirmation': 'gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'USA',
            'state': 'Minnesota',
            'city': 'Minneapolis',
            'postal_code': '55415',
            'street_address': 'Jakobsgatan 10'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # 4: ['UserOne{}', 'Christopher', 'Smith1', 'Megacorp', 3, 'gmail.com', 'email@gmail.com', 'pass', 'passord1', 'Phone number', 'USA', 'Minnesota', 'Minneapolis', '111 52', '401 Chicago Avenue'] invalid form
    def test_two_way_four(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne{}',
            'first_name': 'Christopher',
            'last_name': 'Smith1',
            'company': 'Megacorp',
            'categories': '3',
            'email': 'gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'pass',
            'password2': 'passord1',
            'phone_number': 'Phone number',
            'country': 'USA',
            'state': 'Minnesota',
            'city': 'Minneapolis',
            'postal_code': '111 52',
            'street_address': '401 Chicago Avenue'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # 5: ['UserTwo', 'Christopher', 'Smith', 'Megacorp', 2, 'email@gmail.com', 'email@gmail.com', 'pass', 'pass', '81549300', 'Sweden', 'N/A', 'Stockholm', '55415', '401 Chicago Avenue'] invalid form
    def test_two_way_five(self):
        self.client.post(reverse('signup'), {
            'username': 'Usertwo',
            'first_name': 'Christopher',
            'last_name': 'Smith',
            'company': 'Megacorp',
            'categories': '2',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'pass',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'Sweden',
            'state': 'N/A',
            'city': 'Stockholm',
            'postal_code': '55415',
            'street_address': '401 Chicago Avenue'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # 6: ['UserOne', 'Christopher1', 'Smith1', '', 1, 'gmail.com', 'gmail.com', 'passord1', 'passord1', 'Phone number', 'Norway', 'N/A', 'Trondheim', '55415', '401 Chicago Avenue'] invalid form
    def test_two_way_six(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '1',
            'email': 'gmail.com',
            'email_confirmation': 'gmail.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': 'Phone number',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '55415',
            'street_address': '401 Chicago Avenue'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # 7: ['UserOne', 'Christopher1', 'Smith1', 'Megacorp', 2, 'email@gmail.com', 'gmail.com', 'pass', 'passord1', 'Phone number', 'USA', 'N/A', 'Stockholm', '7013', 'Jakobsgatan 10'] invalid form
    def test_two_way_seven(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': 'Megacorp',
            'categories': '2',
            'email': 'email@gmail.com',
            'email_confirmation': 'gmail.com',
            'password1': 'pass',
            'password2': 'passord1',
            'phone_number': 'Phone number',
            'country': 'USA',
            'state': 'N/A',
            'city': 'Stockholm',
            'postal_code': '7013',
            'street_address': 'Jakobsgatan 10'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # 8: ['UserTwo', 'Christopher', 'Smith', '', 1, 'gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'Minnesota', 'Stockholm', '111 52', 'Jakobsgatan 10'] invalid form
    def test_two_way_eight(self):
        self.client.post(reverse('signup'), {
            'username': 'UserTwo',
            'first_name': 'Christopher',
            'last_name': 'Smith',
            'company': '',
            'categories': '1',
            'email': 'gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'Minnesota',
            'city': 'Stockholm',
            'postal_code': '111 52',
            'street_address': 'Jakobsgatan 10'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #    9: ['UserOne{}', 'Christopher', 'Smith', '', 1, 'gmail.com', 'gmail.com', 'pass', 'pass', 'Phone number', 'Sweden', 'N/A', 'Trondheim', '7013', 'Jakobsgatan 10'] invalid form
    def test_two_way_nine(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne{}',
            'first_name': 'Christopher',
            'last_name': 'Smith',
            'company': '',
            'categories': '1',
            'email': 'gmail.com',
            'email_confirmation': 'gmail.com',
            'password1': 'pass',
            'password2': 'pass',
            'phone_number': 'Phone number',
            'country': 'Sweden',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '7013',
            'street_address': 'Jakobsgatan 10'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   10: ['UserTwo', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'passord1', 'Phone Number', 'Sweden', 'N/A', 'Minneapolis', '7013', 'Munkegata 1'] invalid form
    @skip('Logically this test should fail because Minneapolis and 7013 is are not city and postal code')
    def test_two_way_ten(self):
        self.client.post(reverse('signup'), {
            'username': 'UserTwo',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '3',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': 'Phone Number',
            'country': 'Sweden',
            'state': 'N/A',
            'city': 'Minneapolis',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   11: ['UserOne', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'pass', 'pass', '81549300', 'Norway', 'N/A', 'Trondheim', '111 52', 'Jakobsgatan 10'] invalid form
    def test_two_way_eleven(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '3',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'pass',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '111 52',
            'street_address': 'Jakobsgatan 10'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   12: ['UserOne{}', 'Christopher1', 'Smith1', '', 2, 'email@gmail.com', 'email@gmail.com', 'passord1', 'passord1', '81549300', 'Norway', 'Minnesota', 'Trondheim', '7013', 'Munkegata 1'] invalid form
    def test_two_way_twelve(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne{}',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '2',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'Minnesota',
            'city': 'Trondheim',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   13: ['UserOne', 'Christopher1', 'Smith1', '', 1, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'USA', 'N/A', 'Minneapolis', '7013', 'Munkegata 1'] invalid form
    def test_two_way_thirteen(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '1',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'USA',
            'state': 'N/A',
            'city': 'Minneapolis',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #    14: ['UserTwo', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'USA', 'N/A', 'Trondheim', '111 52', 'Jakobsgatan 10'] invalid form
    def test_two_way_fourteen(self):
        self.client.post(reverse('signup'), {
            'username': 'UserTwo',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '3',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'USA',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '111 52',
            'street_address': 'Jakobsgatan 10'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   15: ['UserOne', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Sweden', 'Minnesota', 'Stockholm', '7013', '401 Chicago Avenue'] invalid form
    def test_two_way_fifteen(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '3',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'Sweden',
            'state': 'Minnesota',
            'city': 'Stockholm',
            'postal_code': '7013',
            'street_address': '401 Chicago Avenue'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   16: ['UserOne{}', 'Christopher1', 'Smith1', '', 2, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'N/A', 'Minneapolis', '55415', 'Munkegata 1'] invalid form
    def test_two_way_sixteen(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne{}',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '2',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Minneapolis',
            'postal_code': '55415',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   17: ['UserOne{}', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'N/A', 'Minneapolis', '7013', 'Munkegata 1'] invalid form
    def test_two_way_seventeen(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne{}',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '3',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Minneapolis',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    #   18: ['UserOne{}', 'Christopher1', 'Smith1', '', 3, 'email@gmail.com', 'email@gmail.com', 'passord1', 'pass', '81549300', 'Norway', 'N/A', 'Stockholm', '7013', 'Munkegata 1'] invalid form
    def test_two_way_eighteen(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne{}',
            'first_name': 'Christopher1',
            'last_name': 'Smith1',
            'company': '',
            'categories': '3',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'pass',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Stockholm',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)


    #=======================================================================================================
    # additional two way tests to test email and password in isolation
    #non-matching passwords
    # 19: ['UserOne', 'Christopher', 'Smith', 'Megacorp', 1, 'email@gmail.com', 'test@gmail.com', 'passord1', 'passord1', '81549300', 'Norway', 'N/A', 'Trondheim', '7013', 'Munkegata 1'] valid_form
    @skip('email addresses are matching, but the form passes')
    def test_two_way_nineteen(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher',
            'last_name': 'Smith',
            'company': 'Megacorp',
            'categories': '1',
            'email': 'email@gmail.com',
            'email_confirmation': 'test@gmail.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # matching but erroneously configured email addresses
    # 20: ['UserOne', 'Christopher', 'Smith', 'Megacorp', 1, 'email@gmail.com', 'test@gmail.com', 'passord1', 'passord1', '81549300', 'Norway', 'N/A', 'Trondheim', '7013', 'Munkegata 1'] valid_form
    def test_two_way_twenty(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher',
            'last_name': 'Smith',
            'company': 'Megacorp',
            'categories': '1',
            'email': 'gmail.com',
            'email_confirmation': 'gmail.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

    # non-matching but long enough password and conf password
    # 21: ['UserOne', 'Christopher', 'Smith', 'Megacorp', 1, 'gmail.com', 'gmail.com', 'passord1', 'passord1', '81549300', 'Norway', 'N/A', 'Trondheim', '7013', 'Munkegata 1'] valid_form
    def test_two_way_twentyone(self):
        self.client.post(reverse('signup'), {
            'username': 'UserOne',
            'first_name': 'Christopher',
            'last_name': 'Smith',
            'company': 'Megacorp',
            'categories': '1',
            'email': 'email@gmail.com',
            'email_confirmation': 'email@gmail.com',
            'password1': 'passord1',
            'password2': 'passord1234',
            'phone_number': '81549300',
            'country': 'Norway',
            'state': 'N/A',
            'city': 'Trondheim',
            'postal_code': '7013',
            'street_address': 'Munkegata 1'
        })
        self.assertEquals(User.objects.all().count(), 0)

# =============================================================================================================
# TASK 3 System robustness test for new features
# =============================================================================================================

class SystemTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.gardening = ProjectCategory.objects.create(name="Gardening")
        self.painting = ProjectCategory.objects.create(name="Painting")
        self.programming = ProjectCategory.objects.create(name="Programming")
        self.client.post(reverse('signup'), {
            'username': 'user1',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email@email.com',
            'email_confirmation': 'email@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': 'city',
            'postal_code': '12345',
            'street_address': 'street'
        })
        self.client.post(reverse('signup'), {
            'username': 'user2',
            'first_name': 'firstname',
            'last_name': 'lastname',
            'company': 'company',
            'categories': '1',
            'email': 'email2@email.com',
            'email_confirmation': 'email2@email.com',
            'password1': 'passord1',
            'password2': 'passord1',
            'phone_number': '12345',
            'country': 'country',
            'state': 'State',
            'city': 'city',
            'postal_code': '12345',
            'street_address': 'street'
        })
        self.assertEqual(User.objects.all().count(), 2)
        self.user_object = User.objects.get(username='user1')
        self.assertIsNotNone(self.user_object)
        self.client.login(username='user1', password='passord1')

    # =============================================================================================================
    # Test profile page view
    # =============================================================================================================

    def test_view_non_existing_character_profile(self):
        try:
            response = self.client.get(reverse('profile', kwargs={'username': '2903r8203fj423e'}))
            self.fail('Should throw exception')
        except Exception:
            pass

    def test_view_non_string_profile(self):
        try:
            response = self.client.get(reverse('profile', kwargs={'username': 123890}))
            self.fail('Should throw exception')
        except Exception:
            pass

    def test_view_non_existing_symbol_profile(self):
        try:
            response = self.client.get(reverse('profile', kwargs={'username': '@@@@@@'}))
            self.fail('Should throw exception')
        except Exception:
            pass

    # =============================================================================================================
    # Test profile rating
    # =============================================================================================================

    def test_rate_profile_num_string(self):
        try:
            self.client.post(reverse('rate_profile', kwargs={'username': 'user2'}), {
                'rating': '3'
            })
            response = self.client.get(reverse('profile', kwargs={'username': 'user2'}))
            self.fail('Should throw exception')
        except Exception:
            pass

    def test_rate_profile_character_string(self):
        try:
            self.client.post(reverse('rate_profile', kwargs={'username': 'user2'}), {
                'rating': 'udwudwuwed'
            })
            response = self.client.get(reverse('profile', kwargs={'username': 'user2'}))
            self.fail('Should throw exception')
        except Exception:
            pass

    def test_rate_profile_invalid_rating(self):
        try:
            self.client.post(reverse('rate_profile', kwargs={'username': 'user2'}), {
                'rating': 7
            })
            response = self.client.get(reverse('profile', kwargs={'username': 'user2'}))
            self.fail('Should throw exception')
        except Exception:
            pass

    def test_rate_own_profile_negative_rating(self):
        self.client.post(reverse('rate_profile', kwargs={'username': 'user2'}), {
            'rating': -3
        })
        response = self.client.get(reverse('profile', kwargs={'username': 'user2'}))
        self.assertNotEquals(response.context['avg_rating'], 0.0)

    def test_rate_own_profile_character_string(self):
        try:
            self.client.post(reverse('rate_profile', kwargs={'username': self.user_object.username}), {
                'rating': 'udwudwuwed'
            })
            response = self.client.get(reverse('profile', kwargs={'username': self.user_object.username}))
            self.fail('Should throw exception')
        except Exception:
            pass

    def test_rate_own_profile_num_string(self):
        try:
            self.client.post(reverse('rate_profile', kwargs={'username': self.user_object.username}), {
                'rating': '3'
            })
            response = self.client.get(reverse('profile', kwargs={'username': self.user_object.username}))
            self.fail('Should throw exception')
        except Exception:
            pass

    def test_rate_own_profile_invalid_rating(self):
        try:
            self.client.post(reverse('rate_profile', kwargs={'username': self.user_object.username}), {
                'rating': 7
            })
            response = self.client.get(reverse('profile', kwargs={'username': self.user_object.username}))
            self.fail('Should throw exception')
        except Exception:
            pass

    # =============================================================================================================
    # Test edit password
    # =============================================================================================================
    def test_change_password(self):
        response = self.client.post(reverse('edit_password', kwargs={'username': self.user_object.username}), {
            'old_password': 'passord1',
            'new_password1': 'passord1234',
            'new_password2': 'passord1234'
        })
        self.assertEquals(response.status_code, 200)

    def test_change_password_symbol_input(self):
        response = self.client.post(reverse('edit_password', kwargs={'username': self.user_object.username}), {
            'old_password': 'passord1',
            'new_password1': '@@@@@@@!',
            'new_password2': '@@@@@@@!'
        })
        self.client.logout()
        self.client.login(username='user1', password='@@@@@@@!')
        self.assertEqual(int(self.client.session['_auth_user_id']), self.user_object.pk)

    def test_change_password_non_string_password(self):
        try:
            response = self.client.post(reverse('edit_password', kwargs={'username': self.user_object.username}), {
                'old_password': 'passord1',
                'new_password1': 1234567,
                'new_password2': 1234567
            })
            self.client.logout()
            self.client.login(username='user1', password=1234567)
            self.assertEqual(int(self.client.session['_auth_user_id']), self.user_object.pk)
            self.fail('The password should not have been changed')
        except Exception:
            pass


    def test_change_password_empty_password(self):
        try:
            response = self.client.post(reverse('edit_password', kwargs={'username': self.user_object.username}), {
                'old_password': 'passord1',
                'new_password1': '',
                'new_password2': ''
            })
            self.client.logout()
            self.client.login(username='user1', password='')
            self.assertEqual(int(self.client.session['_auth_user_id']), self.user_object.pk)
            self.fail('The password should not have been changed')
        except Exception:
            pass

