from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', auth_views.LoginView.as_view(template_name='user/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    path('signup/', views.signup, name='signup'),
    path('<username>/rate_profile/', views.rate_profile_view, name='rate_profile'),
    path('<username>/', views.profile_view, name='profile'),
    path('<username>/edit_password', views.edit_password_view, name='edit_password'),
]
